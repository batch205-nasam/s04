class Student {
	constructor(name,email,grades) {
		this.name = name;
		this.email = email;
		this.average = undefined;
		this.isPassed = undefined;
		this.isPassedWithHonors = undefined;

		if(grades.some(grade => typeof grade !== 'number')){
			this.grades = undefined
		} else{
			this.grades = grades;
		}
	}

	login(){

        console.log(`${this.email} has logged in`);
        return this;
    }

    logout(){
        console.log(`${this.email} has logged out`);
        return this;
    }

    listGrades(){
        this.grades.forEach(grade => {
            console.log(grade);
        })
        return this;
    }

    computeAve(){
        let sum = this.grades.reduce((accumulator,num) => accumulator += num)
        this.average = Math.round(sum/4);
        return this;
    }

    willPass() {
        this.isPassed = Math.round(this.computeAve().average) >= 85 ? true : false;
        return this;
    }

    willPassWithHonors() {

       this.isPassedWithHonors = this.willPass().isPassed && Math.round(this.computeAve().average) >= 90 ? true : this.willPass().isPassed ? false : undefined;
       return this;
    }
}


// Class section will allow us to group our students as section

class Section {

	// every instance of Section class with be instatiated with an empty array for our students

	constructor(name){

		this.name = name;
		this.students = [];
		// add a counter or number of honor students
		this.honorStudents = undefined;
		this.passedStudents = undefined;
		this.sectionAve = undefined;

	}

	addStudent(name,email,grades){

		// A student instance/object will be instantiated with the name, email, grades, and push into our students property

		this.students.push(new Student(name,email,grades));
		return this;
	}

	// countHonorStudents will loop over each Student instance in our students array and count the number of students who will pass with honors
	countHonorStudents(){
		// accumulate the number of honor students
		let count = 0;

		// loop over each Student instance in the students array
		this.students.forEach(student => {
			// log the student instance currently being iterated
			// console.log(student);

			// log if each student's isPassedWithHonors propery.

			// console.log(student.willPassWithHonors().isPassedWithHonors);

			// invoke will pass with honors so we can determine and add whether the student pass with honors in its property
			student.willPassWithHonors();
			// console.log(student);

			// Check if student is isPassedWithHonor. IF it is, add 1 to a temporary variable to hold the number of honor students

			if(student.isPassedWithHonors){
				count++
			};
		})
		// update the honorStudents property with the updated value of count

		this.honorStudents = count;
		return this;
	}

// Get the Number of Students
	getNumberOfStudents(){
		let studentCount = 0;
		this.students.forEach(student =>{
			studentCount++
		});

		return studentCount
	}

// count the number of Passed Students
	countPassedStudents(){
		let passedCount = 0;
		this.students.forEach(student => {

			if(student.isPassed){
				passedCount++
			};
		});
		this.passedStudents = passedCount;
		return this;
	}

// Compute Section Ave

	computeSectionAve(){
		let sum = 0;
		this.students.forEach(student =>{
			sum += student.computeAve().average;
		});

		this.sectionAve = sum/this.getNumberOfStudents();
		return this
	}
}

let section1A = new Section("Section1A");
console.log(section1A);

section1A.addStudent("Joy","joy@email.com",[89,91,92,88]);
section1A.addStudent("Jeff","jeff@email.com",[81,80,82,78]);
section1A.addStudent("John","john@email.com",[91,90,92,96]);
section1A.addStudent("Jack","jack@email.com",[95,92,92,93]);

console.log(section1A);

// Check details of John from section1A
// console.log(section1A.students[2]);

// // Check the average of John from section1A
// console.log("John's average:", section1A.students[2].computeAve().average);

// // Check if Jeff paased
// console.log("Did Jeff pass?", section1A.students[1].willPass().isPassed);

section1A.countHonorStudents();

// check the number of honor students:
console.log(section1A);

// ACTIVITY

// Add a new Student
section1A.addStudent("Alex","alex@gmail.com",[84,85,85,86]);

// Get the Number of Students
console.log("Number of Students in Section1A:",section1A.getNumberOfStudents());

// count the number of Passed Students
section1A.countPassedStudents();
console.log(section1A);

// Compute Section Ave
section1A.computeSectionAve();
// console.log(section1A);